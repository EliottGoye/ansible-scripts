Installation de Cacti sur Debian Jessie
==============

[TOC]

## Configuration de la machine

### Interfaces

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

auto eth1
iface eth1 inet static
        address 192.168.1.1
        network 192.168.1.0
        netmask 255.255.255.0
```

### Dépots

```
apt-get update && apt-get upgrade
```

## Installation de Cacti 0.8.8h

### Mise en place

* Création de l'utilisateur `cacti` :

```
adduser cacti
```

* Installation des dépendances :

```bash
apt-get install apache2 mysql-server php5 php5-mysql php5-cgi php5-cli php5-snmp php-pear snmp snmpd libapache2-mod-php5 rrdtool
```

> Note : après l’installation de la base de données, renseigner le mot de passe dans le fichier ~/.pdb et lui appliquer un chmod 600

* Téléchargement de l'archive

```
cd /var/www/
wget http://www.cacti.net/downloads/cacti-0.8.8h.tar.gz
```

* Décompression

```
tar xzvf cacti-*.tar.gz
mv cacti-0.8.8h cacti
rm cacti-*.tar.gz
cd cacti
```

### Mise en place de la base de données

> Note : Créer un fichier `~/.pdb` contenant le mot de passe de la base de données et lui appliquer un `chmod 600 ~/.pdb`

* Connexion à la base

```
mysql -p`cat ~/.pdb`
```

* Création de la base et de l'utilisateur

```
CREATE DATABASE cacti;
GRANT ALL ON cacti.* TO cacti@localhost IDENTIFIED BY 'cacti';
exit;
```

* Import de la base de données

```
mysql -p`cat ~/.pdb` cacti < cacti.sql
```

### Configuration de Cacti

* Éditer `include/config.php` :

```php
$database_type = "mysql";
$database_default = "cacti";
$database_hostname = "localhost";
$database_username = "cacti";
$database_password = "cacti";
```

* Permissions à cacti :

```
chown -R cacti rra/ log/
```

### Configuration de Apache

Éditer `/etc/apache2/sites-available/cacti.conf`

```
<VirtualHost *:80>
        ServerName cacti.local
        DocumentRoot /var/www/html/cacti

        ErrorLog ${APACHE_LOG_DIR}/cacti_error.log
        CustomLog ${APACHE_LOG_DIR}/cacti_access.log combined
</VirtualHost>
```

* Activer la config et relancer Apache

```
a2ensite cacti
service apache2 restart
```

### Édition du crontab

Utiliser `crontab -e` et ajouter la ligne :

```
*/5 * * * * cacti php /var/www/html/cacti/poller.php > /dev/null 2>&1
```

## Reset du mot de passe par défaut

* Se connecter à la base de données :

```
mysql -u cacti -pcacti
```

* Changer le mot de passe

```sql
update user_auth set password=md5('admin') where username='admin';
```