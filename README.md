README
========

## Installation d'Ansible

> :bulb: Note : Pour avoir les droits superutilisateur, lancer `sudo -s` et entrez votre mot de passe

```
apt-get install software-properties-common
apt-add-repository ppa:ansible/ansible
apt-get update
apt-get install ansible
```

## Lancement d'un script en local

Se placer dans le dossier du script et lancer :

```
ansible-playbook -i "localhost," -c local script.yml
```