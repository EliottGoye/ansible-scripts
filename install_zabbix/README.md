Installation de Zabbix
=============

*Eliott Goye - Christophe Malhaire*
*LP ASUR - Groupe 2*

[TOC]

## Configuration des interfaces

```
auto lo
iface lo inet loopback

auto eth0
iface eth0 inet dhcp

auto eth1
iface eth1 inet static
        address 192.168.1.1
        network 192.168.1.0
        netmask 255.255.255.0
```

## Installation de Zabbix 3.2


### Installation

* Téléchargement du paquet :

```
 wget http://repo.zabbix.com/zabbix/3.2/debian/pool/main/z/zabbix-release/zabbix-release_3.2-1+jessie_all.deb
```

* Ajout du dépôt :

```
dpkg -i zabbix-release_3.2-1+jessie_all.deb
apt-get update
```

* Installation :

```
apt-get install zabbix-server-mysql zabbix-frontend-php zabbix-agent
```

> Note : après l’installation de la base de données, renseigner le mot de passe dans le fichier ~/.pdb et lui appliquer un chmod 600

### Mise en place BDD

* Connexion

```
mysql -u root -p`cat ~/.pdb`
```

* Création de la BDD

```
CREATE DATABASE zabbix;
```

* Création de l’utilisateur et privilèges

```
GRANT all privileges on zabbix.* to zabbix@localhost identified by 'zabbix';
exit;
```

* Import du dump de la base de données

```
zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -u zabbix -pzabbix zabbix
```

### Configuration du serveur Web

* Ajouter dans le fichier `/etc/apache2/conf-available/zabbix-server.conf`

```
<IfModule mod_alias.c>
    Alias /zabbix /usr/share/zabbix
</IfModule>


<Directory /usr/share/zabbix>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride None
        Order allow,deny
        allow from all
</Directory>
```

* Activation de la config

```

a2enconf zabbix-server
```

* Editer le fichier `/etc/php5/apache2/php.ini`

```
max_execution_time = 600
max_input_time = 600
memory_limit = 256M
post_max_size = 32M
upload_max_filesize = 16M
date.timezone = "Europe/Paris"
```

* Redémarrage d’Apache et de Zabbix

```
service apache2 restart
```

### Configuration du Zabbix server

* Éditer `/etc/zabbix/zabbix_server.conf :`

```
DBHost=localhost
DBName=zabbix
DBUser=zabbix
DBPassword=zabbix
```

* Activer les daemons au démarrage :

```
systemctl enable zabbix-server
systemctl enable zabbix-agent
```

* Redémarrer le zabbix-server

`service zabbix-server restart`

### Finalisation installation

![zabbix1](https://lh3.googleusercontent.com/-J8__uniTIew/WAddeAQZD-I/AAAAAAAADBs/oNssSOtCERwnFsZrfOrLQM1eT_wOfqyiACLcB/s0/zabbix31.PNG)

![zabbix2](https://lh3.googleusercontent.com/-Pa6aNYAXyEI/WAddjJ52XhI/AAAAAAAADB0/mLV03qGRZEUZAnblNyQIRaJBHa0NmW8lwCLcB/s0/zabbix32.PNG)

![zabbix3](https://lh3.googleusercontent.com/-oQ8Zzf8QEIg/WAddm4_XJaI/AAAAAAAADB8/JkzMaitxWq0XQQdNpas9lEHH-gO58SemwCLcB/s0/zabbix33.PNG)

![zabbix4](https://lh3.googleusercontent.com/-g5r8X79bOyc/WAddr0FvfGI/AAAAAAAADCE/x0gCnyNry2ASj3SCnzTYj8UCKWeqSKSlwCLcB/s0/zabbix34.PNG)

![zabbix5](https://lh3.googleusercontent.com/-lNtAd1BQ8TY/WAddvmgrR9I/AAAAAAAADCM/K041Y1Pe5oor4MblFQhw4xtq3wDTJrfrACLcB/s0/zabbix35.PNG)


### Supervision d'une machine Linux

#### Sur la machine à superviser

* Téléchargement du paquet :

```
 wget http://repo.zabbix.com/zabbix/3.2/debian/pool/main/z/zabbix-release/zabbix-release_3.2-1+jessie_all.deb
```

* Ajout du dépôt :

```
dpkg -i zabbix-release_3.2-1+jessie_all.deb
apt-get update
```

* Installation :

```
apt-get install zabbix-agent
```

* Activation du daemon au démarrage :

```
systemctl enable zabbix-agent
```

* Éditer le fichier `/etc/zabbix/zabbix_agentd.conf` et définir l'IP du serveur de supervision :

```
Server=127.0.0.1,192.168.1.1
```

* Redémarrage du daemon :

```
service zabbix-agent restart
```

#### Sur le serveur de supervision

Sur l'interface web :

**Configuration** --> **Hosts** --> *Create Host*

Définir le nom, le groupe et l'IP du serveur à superviser :

![zabbix36](https://lh3.googleusercontent.com/-yLRh0UuLqIM/WAnIBUrsy-I/AAAAAAAADD0/4W0964ZCT9oGydm9-TyI01lKCgztoh1AQCLcB/s0/zabbix36.PNG "zabbix36.PNG")

Ajout du template OS Linux

![zabbix37](https://lh3.googleusercontent.com/-14kUmlP41s0/WAnIHOtSQ_I/AAAAAAAADD8/0fzkClquOv07mJ0i9nf-FHOdiOBKZAd_ACLcB/s0/zabbix37.PNG "zabbix37.PNG")


### Supervision d'une machine Windows

#### Sur la machine à superviser

* Désactiver le pare feu
* Définir l'IP
* Télécharger Zabbix agent 3.2 ([www.zabbix.com/downloads](www.zabbix.com/downloads))
* Décompresser l'archive

* Déplacer le fichier de config dans `C:` et le renommer en `zabbix_agentd.conf` et l'éditer :

```
Server=127.0.0.1,192.168.1.1
```

* En terminal, se placer dans le dossier contenant le binaire `zabbix_agentd.exe` et lancer les commandes suivantes :

```
zabbix_agentd.exe --install
zabbix_agentd.exe --start
```

#### Sur le serveur de supervision

Config du nom, de l'IP et du groupe :

![zabbix38](https://lh3.googleusercontent.com/-vK4rd_rfjgY/WAnOCOPUPKI/AAAAAAAADEg/ola5Um_cWaQf0h8riu_UQHe1DXLKMo2-gCLcB/s0/zabbix38.PNG "zabbix38.PNG")

Ajout du template Windows:

![zabbix39](https://lh3.googleusercontent.com/-Aa6BS9UGYDk/WAnOGmpFBkI/AAAAAAAADEo/5ECNJ5xXJLIcck4-mVq10xIYPxVLCK6MACLcB/s0/zabbix39.PNG "zabbix39.PNG")

Affichage des graphiques

![zabbix310](https://lh3.googleusercontent.com/-F5nIfeu4nYA/WAnSn-KXS_I/AAAAAAAADFA/h1tVuz3660cjhCN9mE5dCMfNtDEA6laegCLcB/s0/zabbix310.PNG "zabbix310.PNG")

## Installation Zabbix 2 (Deprecated)

> /!\ Ce qui suit concerne une version dépréciée de Zabbix

* Téléchargement depuis les dépôts

```
apt-get install zabbix-server-mysql zabbix-frontend-php zabbix-agent php5-mysql
```

> Note : après l'installation de la base de données, renseigner le mot de passe dans le fichier `~/.pdb` et lui appliquer un `chmod 600`

* Activation du lancement au démarrage

```
sed -i -e 's/START\=no/START\=yes/g' /etc/default/zabbix-server
```

> Note : cette commande fait une substitution sur l'expression `START=no` et la remplace par `START=yes`

### Base de données

* Connexion

```
mysql -u root -p`~/.pdb`
```
* Création de la BDD

```
CREATE DATABASE zabbix;
```

* Création de l'utilisateur et privilèges

```
GRANT all privileges on zabbix.* to zabbix@localhost identified by 'zabbix';
exit;
```

* Import des dumps de la base de données

```
zcat /usr/share/zabbix-server-mysql/schema.sql.gz | mysql -u zabbix -pzabbix zabbix
zcat /usr/share/zabbix-server-mysql/images.sql.gz | mysql -u zabbix -pzabbix zabbix
zcat /usr/share/zabbix-server-mysql/data.sql.gz | mysql -u zabbix -pzabbix zabbix
```

### Config et redémarrage de Zabbix

* Éditer `/etc/zabbix/zabbix_server.conf`

```
DBHost=localhost
DBName=zabbix
DBPassword=zabbix
DBSocket=/var/run/mysqld/mysqld.sock
DBPort=3306
```

* Relancer le daemon

```
service zabbix-server restart
```

### Configuration de l'interface

* Ajouter dans le fichier `/etc/apache2/conf-available/zabbix-server.conf`

```
<IfModule mod_alias.c>
    Alias /zabbix /usr/share/zabbix
</IfModule>


<Directory /usr/share/zabbix>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride None
        Order allow,deny
        allow from all
</Directory>
```

* Activation de la config

```
a2enconf zabbix-server
```

* Editer le fichier `/etc/php5/apache2/php.ini`

```
max_execution_time = 600
max_input_time = 600
memory_limit = 256M
post_max_size = 32M
upload_max_filesize = 16M
date.timezone = "Europe/Paris"
```

* Redémarrage d'Apache

```
service apache2 restart
```

### Fin de la configuration

Se rendre sur l'URL `http://{Ip_Serveur}/zabbix/setup.php`


![zabbix1](https://lh3.googleusercontent.com/-Ax1maqloGX8/V_yV1FpXC0I/AAAAAAAAC3U/4NPykXfWGZA_cUDN_r6xjCvRla2UnIk3ACLcB/s0/zabbix1.PNG "zabbix1.PNG")
*Écran d'accueil de l'install de Zabbix*


![zabbix2](https://lh3.googleusercontent.com/-Aca6RtRJNTE/V_yV6dsQnwI/AAAAAAAAC3c/BV2PfOgpJ-EokRVVdlbKgNNe7cZFGfBpgCLcB/s0/zabbix2.PNG "zabbix2.PNG")
*Vérification des paramètres PHP*

![zabbix3](https://lh3.googleusercontent.com/-DrVaBngWdv4/V_yV_3WIbpI/AAAAAAAAC3k/c44qO060pMcBd22u2pC54NBWORZqtV6oACLcB/s0/zabbix3.PNG "zabbix3.PNG")
*Configuration de la connexion à la BDD*

![zabbix4](https://lh3.googleusercontent.com/-8pQ6ZUv539g/V_yWEQ6r-4I/AAAAAAAAC3s/Qoh8GpCTm34Eh9jH-GEAgfBhjOAgLXiAgCLcB/s0/zabbix4.PNG "zabbix4.PNG")
*Configuration des détails serveur*

![zabbix5](https://lh3.googleusercontent.com/-QoRtJJTMBGg/V_yWJ_dvNcI/AAAAAAAAC30/ekcR7UKthaQqlYfuSD33qvKDk7TQry9IwCLcB/s0/zabbix5.PNG "zabbix5.PNG")
*Résumé*

![zabbix6](https://lh3.googleusercontent.com/-NpldFZyj_Mc/V_yWPRv2phI/AAAAAAAAC38/aKXAZZXVT-ccq-eRUU21vAXrlQ5ey5iRQCLcB/s0/zabbix6.PNG "zabbix6.PNG")

Si le dernier écran affiche une erreur comme sur la capture, ajouter le fichier `/etc/zabbix/zabbix.conf.php` avec le contenu suivant du fichier PHP proposé :

```
<?php
// Zabbix GUI configuration file
global $DB;

$DB['TYPE']     = 'MYSQL';
$DB['SERVER']   = 'localhost';
$DB['PORT']     = '0';
$DB['DATABASE'] = 'zabbix';
$DB['USER']     = 'zabbix';
$DB['PASSWORD'] = 'zabbix';

// SCHEMA is relevant only for IBM_DB2 database
$DB['SCHEMA'] = '';

$ZBX_SERVER      = 'localhost';
$ZBX_SERVER_PORT = '10051';
$ZBX_SERVER_NAME = 'zabbix';

$IMAGE_FORMAT_DEFAULT = IMAGE_FORMAT_PNG;
?>
```

![zabbix7](https://lh3.googleusercontent.com/-OBPrri33x5g/V_yWWMnlLpI/AAAAAAAAC4E/NoSQZnKjaq0Yk7IQMSX4aH521hFsxsrXwCLcB/s0/zabbix7.PNG "zabbix7.PNG")

Zabbix est installé !

> user : `admin`
> pass : `zabbix`

### Supervision d'une machine Linux

#### Sur la machine à superviser :
```
apt-get install zabbix-agent
```

Editer le fichier `/etc/zabbix/zabbix_agentd.conf` et définir l'IP du serveur de supervision :

```
Server=127.0.0.1,192.168.1.1
```

#### Sur le serveur de supervision

Sur l'interface Web, allez dans l'onglet **Configuration --> Hosts** et cliquer sur **Create host**

* Renseigner le nom de l'hôte et son IP

![zabbix8](https://lh3.googleusercontent.com/-VsMgHpmL9VM/WADw5z7ABvI/AAAAAAAAC88/wnGtrs2FmGIOn-LHqOYf7WhNTxzDt7DcQCLcB/s0/zabbix8.PNG "zabbix8.PNG")

* Dans l'onglet template, ajouter le template pour l'OS Linux

![zabbix9](https://lh3.googleusercontent.com/-qfw-PFnctH8/WADxDHuD6VI/AAAAAAAAC9E/2RrIIyJkVgc0aaYOs4FIzPs3glW8zfVrACLcB/s0/zabbix9.PNG "zabbix9.PNG")